//
//  Model.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation
import UIKit

// MARK: - School
struct School: Identifiable {
    let id: String
    let name: String
    let phone: String
    let location: String
}

// MARK: Codable
extension School: Codable {
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case phone = "phone_number"
        case location
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        phone = try container.decode(String.self, forKey: .phone)
        location = try container.decode(String.self, forKey: .location)
    }
}

// MARK: - School.Details
extension School {
    struct Details: Identifiable {
        let id: String
        let name: String
        let testTakers: String
        let readingScore: String
        let mathScore: String
        let writingScore: String
    }
}

extension School.Details: Decodable {
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case testTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScroe = "sat_writing_avg_score"

    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        testTakers = try container.decode(String.self, forKey: .testTakers)
        readingScore = try container.decode(String.self, forKey: .readingScore)
        mathScore = try container.decode(String.self, forKey: .mathScore)
        writingScore = try container.decode(String.self, forKey: .writingScroe)
    }
}
