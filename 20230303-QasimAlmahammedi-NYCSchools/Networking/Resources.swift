//
//  NetworkRequest.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation

// MARK: - APIResource
protocol APIResource {
    associatedtype ModelType: Decodable
    var path: String { get }
    var parameters: [String: String] { get }
}

extension APIResource {
    var url: URL {
        URL(string: "https://data.cityofnewyork.us/resource")!
            .appendingPathComponent(path)
            .appendingParameters(parameters)
            .appendingPathExtension("json")
    }
}

// MARK: - SchoolsResource
struct SchoolsResource: APIResource {
    typealias ModelType = School
    var id: String?

    var path: String {
        return "s3k6-pzi2"
    }

    var parameters: [String : String] {
        guard let id = id else { return [:] }
        return [ "dbn": id]
    }
}

// MARK: - SchoolDetailsResource
struct SchoolDetailsResource: APIResource {
    typealias ModelType = School.Details
    var id: String

    var path: String {
        return "f9bf-2cp4"
    }

    var parameters: [String : String] {
        return [ "dbn": id]
    }
}
