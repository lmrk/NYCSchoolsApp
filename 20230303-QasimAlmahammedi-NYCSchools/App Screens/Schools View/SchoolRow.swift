//
//  SchoolViewRow.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import SwiftUI

// MARK: - SchoolRow
struct SchoolRow: View {
    let id: String
    let name: String
    let phone: String
    let location: String

    var body: some View {
        VStack(alignment: .leading, spacing: 8.0) {
            Text(name)
                .font(.headline)
            Text(location)
                .font(.footnote)
                .bold()
                .foregroundColor(.accentColor)
            Text(id)
                .font(.caption)
                .foregroundColor(.secondary)
            ZStack(alignment: Alignment(horizontal: .leading, vertical: .center)) {
                Label("\(phone)", systemImage: "phone")
                Label("\(id)", systemImage: "tag")
                    .padding(.leading, 204.0)
            }
            .foregroundColor(.teal)
        }
        .padding(.top, 24.0)
        .padding(.bottom, 16.0)
    }
}

extension SchoolRow {
    init(school: School) {
        self.init(
            id: school.id,
            name: school.name,
            phone: school.phone,
            location: school.location)
    }
}

