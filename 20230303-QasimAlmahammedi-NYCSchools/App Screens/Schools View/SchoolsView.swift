//
//  ContentView.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import SwiftUI

struct SchoolsView: View {
    @EnvironmentObject private var stateController: StateController
    @StateObject private var viewModel = ViewModel()
    @State private var isErrorAlertPresented = false

    var body: some View {
        
        Content(schools: $stateController.topSchools)
            .loading(viewModel.isLoading)
            .onAppear {
                Task { await update() }
            }
            .refreshable { await update() }
            .alert(isPresented: $isErrorAlertPresented) {
                Alert(title: Text("There was an error"), message: Text("Please try again later"))
            }
        
    }
}

private extension SchoolsView {
    struct Content: View {
        @Binding var schools: [School]
        var body: some View {

            List {
                ForEach(schools) { school in
                    NavigationLink(destination: SchoolDetailsView(id: school.id)) {
                        SchoolRow(school: school)
                    }
                }
                .onDelete(perform: deleteItems(atOffsets:))
                .onMove(perform: move(fromOffsets:toOffset:))
            }
            .listStyle(.automatic)
            .navigationTitle("NYC High Schools")

        }

        // List delete and move items.
        func deleteItems(atOffsets offsets: IndexSet) {
            schools.remove(atOffsets: offsets)
        }

        func move(fromOffsets source: IndexSet, toOffset destination: Int) {
            schools.move(fromOffsets: source, toOffset: destination)
        }
    }
}

private extension SchoolsView {
    func update() async {
        guard let items = await viewModel.fetchAllSchools() else {
            isErrorAlertPresented = true
            return
        }
        // Save the newly fetched schools to disk.
        stateController.topSchools = items
    }
}


struct SchoolsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsView()
            .loading(true)
    }
}
