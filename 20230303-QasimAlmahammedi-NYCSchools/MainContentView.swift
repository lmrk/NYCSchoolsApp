//
//  MainContentView.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import SwiftUI

struct MainContentView: View {
    @State private var isLoggingIn = false
    @EnvironmentObject private var settingsController: SettingsController
    @AppStorage(NYCSchoolsApp.Keys.isLoggedIn) private var isLoggedIn = false
    @SceneStorage("ContentView.SelectedTab") private var selectedTab = 0
    
    var body: some View {
        TabView(selection: $selectedTab) {
            NavigationView {
                SchoolsView()
            }
            .tabItem { Label("NYC Schools", systemImage: "list.number") }
            .tag(0)
            NavigationView {
                SettingsView()
            }
            .tabItem { Label("Settings", systemImage: "gear") }
            .tag(1)
        }
        .accentColor(settingsController.theme.accentColor)
        .environment(\.theme, settingsController.theme)
        .onAppear { isLoggingIn = !isLoggedIn }
        .fullScreenCover(isPresented: $isLoggingIn) {
            LoginView()
                .accentColor(settingsController.theme.accentColor)
        }
    }
}

struct MainContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainContentView()
    }
}
