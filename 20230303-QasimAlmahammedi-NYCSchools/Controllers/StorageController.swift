//
//  StorageController.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation

class StorageController {
    private let documentsDirectoryURL = FileManager.default
        .urls(for: .documentDirectory, in: .userDomainMask)
        .first!

    func save(topSchools: [School]) {
        if let data = try? PropertyListEncoder().encode(topSchools) {
            try? data.write(to: schoolsFileURL)
        }
    }

    func fetchTopSchools() -> [School]? {
        guard let data = try? Data(contentsOf: schoolsFileURL) else { return nil }
        return try? PropertyListDecoder().decode([School].self, from: data)
    }
}

private extension StorageController {
    var schoolsFileURL: URL {
        documentsDirectoryURL
            .appendingPathComponent("School")
            .appendingPathExtension("plist")
    }

}
